#include <WiFiScan.h>
#include <WiFiMulti.h>
#include <WiFiSTA.h>
#include <WiFiClient.h>
#include <WiFiAP.h>
#include <WiFiUdp.h>
#include <WiFiGeneric.h>
#include <WiFiType.h>
#include <ETH.h>
#include <WiFi.h>
#include <WiFiServer.h>

const char* ssid     = "BDAG";
const char* password = "bdag2018";
String cabecalho = "";

WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);
  Serial.println('\n');

  pinMode(2, OUTPUT);
  
  WiFi.begin(ssid, password);
  Serial.print("Connecting to ");
  Serial.print(ssid);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print('.');
  }

  Serial.println('\n');
  Serial.println("Connection established!");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());
  
  server.begin();
}

void loop() { 
    //Verifica se existe algum client na rede  
    WiFiClient client = server.available();  
    //Caso positivo, imprime "Novo Client" no monitor  
    if (client){ 
        Serial.println("Novo Client");  
        //Enquanto client conectado, verifica se existem bytes a serem lidos 
        //e concatena os bytes recebidos na String cabecalho; 
        while (client.connected()){ 
           if (client.available()){ 
              cabecalho += (char)client.read();  
              //Se receber nova linha em branco, encerrou leitura dos dados  
              if (cabecalho.endsWith("\n\r\n")){  
                  Serial.println(cabecalho); //imprime cabeçalhos http recebidos 
                  //iniciamos a resposta http com o código OK(200), 
                  //o tipo de conteúdo a ser enviado e tipo de conexão. 
                  client.println("HTTP/1.1 200 OK"); 
                  client.println("Content-Type:text/html"); 
                  client.println("Connection: close"); 
                  client.println(); 
                  if(cabecalho.indexOf("GET /LED_ON")>= 0){ 
                    digitalWrite(2, true); //Acende o LED 
                  }else if(cabecalho.indexOf("GET /LED_OFF")>= 0){ 
                    digitalWrite(2, false); //Apaga o LED 
                  }
                  break; //sai do while loop  
              }  
           } 
        }  
        cabecalho = ""; //ao encerrar conexão, limpa variável cabecalho 
        client.flush(); client.stop(); 
        Serial.println("Client desconectado."); Serial.println(); 
    } 
} 
